import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class DisplayService {
  data;
  states;
  primaryBeneficiary;
  
  url = 'http://192.168.2.143:8008/program/list1/';
  statesData;
  beneficiaryData;
  constructor(public http :Http) { 
    this.statesData= this.getStates();
    this.beneficiaryData = this.getPrimeBenficiary();
  }

  postTest(data){
    let str = "";
    
    if(data=='first_load'){
      str = 'select * from newschema1.projectmanagement_program'
    }else{
      str = `select * from newschema1.projectmanagement_program where ${data}`;
    }
    
    const queryConst = {'query' :str};

    return this.postRequest(queryConst).map(
      (response: Response) => {
        this.data = response.json();
        return this.data;
      }
    );
  }

  postRequest(queryConst) {
    return this.http.post(this.url, queryConst);
    
  }

  getTest() {
    return this.getRequest().map(
      (response: Response) => {
      this.data = response.json();
      return this.data;
      }
    );
  }

  getRequest(): Observable<any>{
    return this.http.get( 'http://139.59.1.249:8080/FamilyApp/webapi/txn/getallplans');
  }

  post(url: string, body: any | null, headers: object = {}): Observable<any> {
    
    return this.http.post(url, body);
  }

  retStates(){
    return this.statesData;
  }

  getStates() {
    return this.getStatesRequest().map(
      (response: Response) => {
        this.states = response.json();
        return this.states;
      }
    );
  }

  getStatesRequest(): Observable<any>{
    return this.http.get('http://192.168.2.143:8008/masterdata/state/list/');
  }

  retPrimeBeneficiary(){
    return this.primaryBeneficiary;
  }
  
  getPrimeBenficiary(){
    return this.getPrimeBenficiaryRequest().map(
      (response: Response) => {
        console.log("Response")
        console.log(response)
        this.primaryBeneficiary = response.json();
        return this.primaryBeneficiary;
      }
    );
  }

  getPrimeBenficiaryRequest(): Observable<any>{
    return this.http.get('http://192.168.2.143:8008/masterdata/primarybeneficiaries/list/');
  }

  // getLocations(){

  // }

  // getLocationsRequest(): Observable<any>{
  //   return this.http.get('')
  // }
}
