import { equal } from "assert";

declare var $: any;
// import * as queryBuilder from 'jQuery-QueryBuilder';
declare var queryBuilder : any;
var temp;
var sql_import_export = 'true';

export class Base {
    build(stats, primaryBeneficiary){

        $(document).ready(function() {
            var rules_basic = {
                condition: 'OR',
                rules: [
                    
                    {
                        id: 'name',
                        operator: 'equal',
                        value: ''
                    }
                ]
              };

            var options = {
                allow_empty: true,
                filters: [
                    {
                        id: 'generic',
                        label: 'Generic',
                        type: 'string',
                    },  
                    {
                        id: 'name',
                        label: 'Name',
                        type: 'string',
                    },  
                    {
                        id: 'program_hosting',
                        label: 'Program type',
                        type: 'string',
                        // type: 'integer',
                        // input: 'select',
                        // values: {
                        //     6: 'Incubator startup program',
                        //     5: 'Incubator program',
                        //     4: 'Response to program',
                        //     2: 'RFP Response',
                        //     1: 'RFP',
                        //     0: 'Normal'
                        // },
                        // operators: ['equal','not_equal']
                    }, 

                    {
                        id: 'primary_beneficiary',
                        label: 'Primary beneficiary',
                        type: 'string'
                        // type: 'integer',
                        // input: 'select',
                        // values: primaryBeneficiary,
                        // operators: ['equal','not_equal']
                    },
                    // {
                    //     id: 'createdBy',
                    //     label: 'Created by',
                    //     type: 'string'
                    // },
                    {
                        id: 'primary_area_of_focus',
                        label: 'Primary area of focus',
                        type:'string',
                        
                        // input: 'select',
                        // values: {
                        //     56: 'Sample Area',
                        //     29: 'Education, literacy, and empowerment',
                        //     28: 'Financial literacy and inclusion',
                        //     27: 'Skills Development, Empowerment, or Livelihood',
                        //     25: 'Community welfare and public infrastructure',
                        //     17: 'Skill Development and Livelihood',
                        //     16: 'Rural development programs',
                        //     15: 'Public libraries',
                        //     14: 'Preventive health care',
                        //     13: 'Poverty eradication',
                        //     12: 'Old age care',
                        //     11: 'National heritage protection',
                        //     10: 'Malnutrition Education',
                        //      9: 'Hunger eradication',
                        //      8: 'Environmental sustainability',
                        //      7: 'Education',
                        //      6: 'Economic Inequality reduction',
                        //      5: 'Armed forces veterans, war widows and their dependents',
                        //      4: 'Animal welfare'
                            //  2: '',
                            //  1: '',
                            //  0: ''
                        // },

                    },
                    {
                        id: 'location',
                        label: 'Location',
                        type: 'string'
                        // type: 'integer',
                        // input: 'select',
                        // values: stats,
                        // operators: ['equal','not_equal']

                        // 
                            // 32: 'Puducherry',
                            // 31: 'Odisha',
                            // 30: 'Nagaland',
                            // 29: 'Mizoram',
                            // 28: 'Meghalaya',
                            // 27: 'Manipur',
                            // 26: 'Madhya Pradesh',
                            // 25: 'Lakshadweep',
                            // 24: 'Kerla',
                            // 23: 'Jharkhand',
                            // 22: 'Jammu & Kashmir',
                            // 21: 'Himachal Pradesh',
                            // 20: 'Haryana',
                            // 19: 'Gujrat',
                            // 18: 'Goa',
                            // 17: 'National Capital Territory of Delhi',
                            // 16: 'Daman and Diu',
                            // 15: 'Dadra and Nagar Haveli',
                            // 14: 'Chhattisgarh',
                            // 13: 'Chandigarh',
                            // 12: 'Bihar',
                            // 11: 'Assam',
                            // 10: 'Arunachal Pradesh',
                            //  9: 'Andhra Pradesh',
                            //  8: 'Andaman and Nicobar Islands',
                            //  5: 'Maharashtra',
                            //  2: 'Karnataka'

                    },
                    {
                        id: 'no_of_beneficiaries',
                        label: 'No of beneficiaries',
                        type: 'integer'
                    }

                ],

                rules: rules_basic
            };

            $('#builder').queryBuilder(options);

            // $('#btn-reset').on('click', function() {
               

            // });
            
            $('#btn-set').on('click', function() {
                $('#builder').queryBuilder('setRulesFromSQL', sql_import_export);
            });

        });
    }

    clickFunc(){
        var result = $('#builder').queryBuilder('getSQL', false, false);
        if (result.sql.length) {
            console.log("SQL "+result.sql)
        return result.sql;
        }
    }


    statesFunctn(states){
        for(var v=0;v<states.length;v++){
            console.log("v "+v[0])
        }
    }

    
}
