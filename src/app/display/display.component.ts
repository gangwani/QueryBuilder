import { DisplayService } from './display.service';
import { Base } from './base';
import { Component, OnInit } from '@angular/core';
import {MatTableModule} from '@angular/material/table';
var data;
declare var $ : any;
declare var queryBuilder : any;

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css'],
  providers: [ DisplayService, Base]
})

export class DisplayComponent implements OnInit {
  query;
  temp;
  filteredData;
  states;
  locations;
  primaryBeneficiary;
  primaryAreaOfFocus;
  
  constructor(public serv : DisplayService, private base : Base) {
    console.log("Constructor")
    var some = "first_load";
    this.serv.postTest(some).subscribe((data) => {
      this.filteredData = data;
      this.serv.getStates().subscribe((data) => {
        this.states = data;
        this.serv.getPrimeBenficiary().subscribe((data) => {
          this.primaryBeneficiary = data;
          this.test();
        })
      })
      
    })
    
    // this.serv.getLocations().subscribe((data) => {
    //   this.locations = data;
    // })
    
  }

  // getStatesFn(){
  //   this.serv.retStates().subscribe((data) => {
  //     this.states = data;
  //   })
  // }

  // getPrimBeneficiary(){
  //   this.serv.retPrimeBeneficiary().subscribe((data) => {
  //     this.primaryBeneficiary = data;
  //   })
  // }
  test() {
    this.query = this.base.build(this.states, this.primaryBeneficiary);
  }
  testClick(){
    let some = this.base.clickFunc();
    this.serv.postTest(some).subscribe((data) => {
      this.filteredData = data;
    })
    // this.serv.getTest().subscribe((data) =>{
    //     console.log(data);
    // });
  }

  resetClick(){
    let some = 'first_load';
    // (<HTMLInputElement>document.getElementById(elementId)).value;
    $('#builder').queryBuilder('reset');
    this.serv.postTest(some).subscribe((data) => {
      this.filteredData = data;
    })
  }

  ngOnInit() {  
    console.log("ngOnInit")
    console.log(this.query);
  }
}